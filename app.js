
function calculateYear() {
    document.getElementById("is_bisiesto_lbl").innerHTML = '';
    var year = document.getElementById("year").value
    var is_bisiesto = false
    if (year % 400 === 0) {
        is_bisiesto = true
    }

    if (year % 4 === 0) {
        is_bisiesto = true
    }

    if (is_bisiesto) {
        document.getElementById("is_bisiesto_lbl").innerHTML = 'Es un año bisisesto';
    } else {
        document.getElementById("is_bisiesto_lbl").innerHTML = 'No es un año bisiesto';
    }
}

function insertRowsAndColumns() {
    var columns = document.getElementById("columns").value;
    var rows = document.getElementById("rows").value;
    var container = document.createElement('table');
    var html_text = '<table>';
    for (var r = 0; r < rows; r++) {
        html_text += '<tr>'
        for (var c = 0; c < columns; c++) {
            html_text += '<th>X</th>';
        }
        html_text += '</tr>';
    }
    html_text += '</table>';
    container.innerHTML = html_text;
    container.setAttribute("border", "2");
    document.getElementById('table').innerHTML = '';
    document.getElementById('table').appendChild(container);
}

function sortArray() {
    var new_arr = []
    for (var x = 0; x < 20; x++) {
        new_arr.push(Math.floor(Math.random() * 100))
    }

    document.getElementById('des_array_lbl').innerHTML = new_arr;
    /*
        Bubble Sort tomado de https://www.delftstack.com/es/howto/javascript/javascript-bubble-sort/
    */
    var length = new_arr.length;
    for (var i = 0; i < length; i++) {
        for (var j = 0; j < (length - i - 1); j++) {
            if (new_arr[j] > new_arr[j + 1]) {
                var tmp = new_arr[j];
                new_arr[j] = new_arr[j + 1];
                new_arr[j + 1] = tmp;
            }
        }
    }
    document.getElementById('ord_array_lbl').innerHTML = new_arr;
}

function operationsWithSets() {
    var set_a = [];
    var set_b = [];
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var charactersLength = characters.length;
    var len_a = 0;
    var len_b = 0;
    while (len_a <= 9) {
        to_insert_in_a = characters.charAt(Math.floor(Math.random() * charactersLength))
        if (!set_a.includes(to_insert_in_a)) {
            set_a.push(to_insert_in_a);

        }
        len_a = set_a.length;
    }

    while (len_b <= 9) {
        to_insert_in_b = characters.charAt(Math.floor(Math.random() * charactersLength))
        if (!set_b.includes(to_insert_in_b)) {
            set_b.push(to_insert_in_b);
        }
        len_b = set_b.length;
    }
    document.getElementById('con_a').innerHTML = set_a;
    document.getElementById('con_b').innerHTML = set_b;
    document.getElementById('union_res_lbl').innerHTML = set_a.concat(
        set_b.filter(
            value => {
                return !set_a.includes(value)
            }
        )
    );
    document.getElementById('inter_res_lbl').innerHTML = set_a.filter(value => set_b.includes(value)); /* se concatenan A y B */
    document.getElementById('dif_res_lbl').innerHTML = set_a.concat(set_b).filter(
        value => set_a.includes(value) && !set_b.includes(value) /* Pertenecen a A pero pertenecen a B */
    );
    var new_set = set_a.concat(set_b)
    document.getElementById('dif_sim_res_lbl').innerHTML = new_set.filter(
        value => !set_a.includes(value) || !set_b.includes(value) /* (pertenecen a A y no pertenecen a B) y (pertenecen a B y mo pertenecen a A) */
    );
}

function requestToBanxico() {
    var NowMoment = moment();
    document.getElementById('result').innerHTML = '';
    start = NowMoment.format('YYYY-MM-DD');
    end = NowMoment.subtract(5, 'd').format('YYYY-MM-DD');
    /* Si le pongo fecha no me trae datos, por esta razon lo dejo sin fecha */
    var url_with_dates = `https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF61745/datos/${start}/${end}?token=e6c7e762b7b20ff2e50467919fd59206827e5216cf6d820c61d8fd4a1b827313`
    var url_with_not_dates = `https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF61745/datos/oportuno?token=e6c7e762b7b20ff2e50467919fd59206827e5216cf6d820c61d8fd4a1b827313`
    $.ajax({
        url: url_with_not_dates,
        jsonp: "callback",
        dataType: "jsonp", //Se utiliza JSONP para realizar la consulta cross-site
        success: function (response) {  //Handler de la respuesta
            var series = response.bmx.series;

            //Se carga una tabla con los registros obtenidos
            for (var i in series) {
                var serie = series[i];
                var reg = '<tr><td>' + serie.titulo + '</td><td>' + serie.datos[0].fecha + '</td><td>' + serie.datos[0].dato + '</td></tr>'
                $('#result').append(reg);
            }
        }
    });
}